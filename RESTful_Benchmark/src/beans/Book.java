package beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Book")
public class Book implements Serializable{
	private static final long serialVersionUID = 1L;
	private String bookNumber;
	private String chapterNumber;
	private String verseNumber;
	private String verseText;
	
	/** Constructor */ 
	public Book() {
		this.bookNumber = "1";
		this.chapterNumber = "1";
		this.verseNumber = "1";
		this.verseText = "";
	}
	
	
	public Book(String bookNumber, String chapterNumber, String verseNumber, String verseText) {
		this.bookNumber = bookNumber;
		this.chapterNumber = chapterNumber;
		this.verseNumber = verseNumber;
		this.verseText = verseText;
	}

	//Setters and Getters
	public String getBookNumber() {
		return bookNumber;
	}


	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}


	public String getChapterNumber() {
		return chapterNumber;
	}


	public void setChapterNumber(String chapterNumber) {
		this.chapterNumber = chapterNumber;
	}


	public String getVerseNumber() {
		return verseNumber;
	}


	public void setVerseNumber(String verseNumber) {
		this.verseNumber = verseNumber;
	}


	public String getVerseText() {
		return verseText;
	}


	public void setVerseText(String verseText) {
		this.verseText = verseText;
	}
	
}
