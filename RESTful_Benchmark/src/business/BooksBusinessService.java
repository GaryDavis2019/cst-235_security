package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.*;
import beans.*;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(BooksBusinessInterface.class)
@LocalBean
@Alternative 

public class BooksBusinessService implements BooksBusinessInterface {
	private List<Book> books = new ArrayList<Book>();
	
    //Constructor that adds testing objects to the orders list
    public BooksBusinessService() {
    	//books.add(new Book("1","1","1","TMP"));
    }

    //Setters and Getters
	@Override
	public List<Book> getBooks() {
		return books;
	}

	@Override
	public void setBooks(List<Book> books) {
		this.books = books;
		
	}

}
