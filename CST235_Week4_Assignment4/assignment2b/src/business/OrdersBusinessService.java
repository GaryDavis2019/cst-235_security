package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.*;


import beans.*;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative 
public class OrdersBusinessService implements OrdersBusinessInterface {
	String orderNumber;
	String productName;
	float price;
	int quantity;
	private List<Order> orders = new ArrayList<Order>();
	
    //Constructor that adds testing objects to the orders list
    public OrdersBusinessService() {
    	orders.add(new Order("OBS","TEST1",1,1));
    	orders.add(new Order("OBS","TEST2",2,2));
    	orders.add(new Order("OBS","TEST3",3,3));
    	orders.add(new Order("OBS","TEST4",4,4));
    	orders.add(new Order("OBS","TEST5",5,5));
    	orders.add(new Order("OBS","TEST6",6,6));
    }

    //Method to print a test message
    public void test() {
        System.out.println("Hello from the OrdersBusinessService");
    }

    //Setters and Getters
	@Override
	public List<Order> getOrders() {
		return orders;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

}
