package controllers;

//import java.io.IOException;

import javax.faces.bean.*;
//import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.*;
import beans.User;
import business.OrdersBusinessInterface;
import javax.ejb.EJB;

@ManagedBean 
@ViewScoped
public class FormController {
	@Inject
	private business.OrdersBusinessInterface service;
	@EJB 
	business.MyTimerService timer;
	
	//Submit button clicked in form and displays the User first and last name
	public String onSubmit(User user) {
		//System.out.println(user.getFirstName()); //print the user first name on console
		//System.out.println(user.getLastName());  //print the user first name on console
		
		service.test(); //prints a test message from either the OrdersBusinessService and AnotherOrdersBusinessService.
		timer.setTimer(10000); //This will display a delayed message 10 seconds after the submit button was pushed.
		
		return "TestResponse.xhtml"; //Forwarding to new form page
		
	}
	
	//Testing redirect
	public String onFlash(User user){
		//ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		System.out.println(FacesContext.getCurrentInstance().getClientIdsWithMessages());
		getRequestMap();
		return "TestResponse.xhtml?faces-redirect=true";
		
	}
	
	public String getRequestMap() {
		return "TestResponse.xhtml";
		
	}
	
	//get the information from the service
	public  OrdersBusinessInterface getService() {
		return service;
	}
}


