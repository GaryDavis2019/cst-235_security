package controllers;

//import java.io.IOException;

import javax.faces.bean.*;
//import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.*;

import beans.Order;
import beans.User;
import business.OrdersBusinessInterface;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//import javax.ejb.EJB;

@ManagedBean 
@ViewScoped
public class FormController {
	@Inject
	private business.OrdersBusinessInterface service;
	//@EJB 
	//business.MyTimerService timer;
	
	//Submit button clicked in form and displays the User first and last name
	public String onLogoff() {
		// Invalidate the Session to clear the security token
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		
		/*ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    String url = ec.getRequestContextPath() + "/TestResponse.xhtml";
	    try {
			ec.redirect(url);
		} catch (IOException e) {
			e.printStackTrace();
		}*/

		// Redirect to a protected page (so we get a full HTTP Request) to get Login Page
		return "TestResponse.xhtml?faces-redirect=true";
	}
	

	
	//Testing redirect
	public String onFlash(User user){
		//ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		System.out.println(FacesContext.getCurrentInstance().getClientIdsWithMessages());
		getRequestMap();
		return "TestResponse.xhtml?faces-redirect=true";
		
	}
	
	public String getRequestMap() {
		return "TestResponse.xhtml";
		
	}
	
	//get the information from the service
	public  OrdersBusinessInterface getService() {
		return service;
	}
	
	private void getAllOrders(){
		List<Order> orders = new ArrayList<Order>();
		Connection conn = null;
		String url = "jdbc:postgresql://localhost:5432/";
		String username = "postgres";
		String password = "password";
		String stringStatement = "SELECT * FROM testapp.ORDERS";
		try {
			conn = DriverManager.getConnection(url, username, password);
			System.out.println("Success");
			Statement statement  = conn.createStatement();
			ResultSet rs = statement.executeQuery(stringStatement);
			while(rs.next()) {
				System.out.println("ID:"+ rs.getString("ORDER_NO")+"; Product:"+rs.getString("PRODUCT_NAME")+"; Price:"+ rs.getFloat("PRICE")
				+ "; QUANT:"+rs.getInt("QUANTITY"));
				orders.add(new Order(rs.getString("ORDER_NO"),rs.getString("PRODUCT_NAME"),rs.getFloat("PRICE"),rs.getInt("QUANTITY")));
				
			}
			service.setOrders(orders);
			rs.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println("Failure");
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
			}
	}
	
	
	public String onSendOrder() {
		service.sendOrder(new Order("001177665544", "This was inserted message", (float)44.13, 100));
		return "OrderResponse.xhtml";
	}
	
	//Direct customer OrderResponse form
	public String onOrderProcessed() {
		getAllOrders();
		return "TestResponse.xhtml"; //Forwarding to new form page
		
	}
	
}


