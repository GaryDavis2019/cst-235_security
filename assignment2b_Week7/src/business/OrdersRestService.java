package business;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.*;

import beans.Order;

@RequestScoped
@Path("/orders")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })


//@XmlRootElement(name="OrdersRestService")
public class OrdersRestService {
	
	@Inject
	private business.OrdersBusinessInterface service;
	
	public OrdersRestService(){
		
	}
	
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Order> getOrdersAsJson(){
		return service.getOrders();
		
	}
	
	@GET
	@Path("/getxml")
	@Produces(MediaType.APPLICATION_XML)
	public Order[] getOrdersAsXml() {
		Order[] tmpOrd = new Order[service.getOrders().size()];
		for (int x=0; x<service.getOrders().size(); x++) {
			tmpOrd[x] = service.getOrders().get(x);
		}
		return tmpOrd;
	}
}
