package beans;
import javax.faces.bean.*;
import javax.validation.constraints.*;


@ManagedBean 
@SessionScoped
public class User {
	@NotNull()
	@Size(min=5, max=15) 
	String firstName;
	@NotNull()
	@Size(min=5, max=15)
	String lastName;

	/** Constructors */ 
	public User(String FirstName, String LastName) { 
		firstName = FirstName;
		lastName = LastName;
	}
	
	public User() { 
		firstName = "Gary";
		lastName = "Davis";
	}
	
	//First Name getter/setter	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String FirstName) {
		firstName = FirstName;
	}
	
	//Last Name getter/setter
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String LastName) {
		lastName = LastName;
	}

}
