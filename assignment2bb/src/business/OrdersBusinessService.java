package business;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.*;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.sql.*;

import beans.*;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative 

public class OrdersBusinessService implements OrdersBusinessInterface {
	int orderNumber;
	String productName;
	float price;
	int quantity;
	private List<Order> orders = new ArrayList<Order>();
	
    //Constructor that adds testing objects to the orders list
    public OrdersBusinessService() {
    	orders.add(new Order("1","TEST1",1,1));
    	orders.add(new Order("2","TEST2",2,2));
    	orders.add(new Order("3","TEST3",3,3));
    	orders.add(new Order("4","TEST4",4,4));
    	orders.add(new Order("5","TEST5",5,5));
    	orders.add(new Order("6","TEST6",6,6));
    }

    /*Removed
    //Method to print a test message
    public void test() {
        System.out.println("Hello from the OrdersBusinessService");
    }*/

    //Setters and Getters
	@Override
	public List<Order> getOrders() {
		return orders;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;
	@Resource(mappedName="java:/jms/queue/Order")
	private Queue queue;
	public void sendOrder(Order order) {
		// Send a Message for an Order
				try 
				{
					Connection connection = connectionFactory.createConnection();
					Session  session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
					MessageProducer messageProducer = session.createProducer(queue);
					TextMessage message1 = session.createTextMessage();
					message1.setText("This is test message");
					messageProducer.send(message1);
					ObjectMessage message2 = session.createObjectMessage();
					message2.setObject(order);
					messageProducer.send(message2);
					connection.close();
				} 
				catch (JMSException e) 
				{
					e.printStackTrace();
				}

	}
	public void insertOrder(Order order) {
		java.sql.Connection conn = null;
		String url = "jdbc:postgresql://localhost:5432/";
		String username = "postgres";
		String password = "password";
		String stringStatement = "INSERT INTO  testapp.ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES(?, ?, ?, ?)";
		try {
			conn = DriverManager.getConnection(url, username, password);
			System.out.println("Success");
			Statement statement  = conn.createStatement();
			PreparedStatement preparedStmt = conn.prepareStatement(stringStatement);
			preparedStmt.setString(1, order.getOrderNumber());
			preparedStmt.setString(2, order.getProductName());
			preparedStmt.setFloat(3, (float)order.getPrice());
			preparedStmt.setInt(4, order.getQuantity());
			preparedStmt.execute();
			
			//statement.executeUpdate(stringStatement);
			statement.close();
			preparedStmt.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println("Failure");
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
			}
	}

}
